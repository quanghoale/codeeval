#include <iostream>
#include <fstream>
#include <string>
#include <sstream> 

int f5(int value) {
//    if (value == 0) 
//        return 0;

    return value / 5 + 1; 
}

int f10 (int value) {
    int factor = value / 10;
    int solution = 0;
    if (factor < 1) {
        return f5(value); 
    } else {
        for (int idx = 0; idx <= factor; idx++) {
            solution += f5(value - idx * 10); 
        }
    }
    return solution;
}


int f25 (int value) {
    int factor = value / 25;
    int solution = 0;
    if (factor < 1) {
        return f10(value); 
    } else {
        for (int idx = 0; idx <= factor; idx++) {
            solution += f10(value - idx * 25); 
        }
    }
    return solution;
}


int f50 (int value) {
    int factor = value / 50;
    int solution = 0;
    if (factor < 1) {
        return f25(value); 
    } else {
        for (int idx = 0; idx <= factor; idx++) {
            solution += f25(value - idx * 50); 
        }
    }
    return solution;
}

int main (int argc, char* argv[]) {
    int value;
    std::ifstream stream(argv[1]); 
    std::string line; 

    while (getline(stream, line)) {
        // C++11/14 - std::stoi
        //value = std::stoi(line);
       std::stringstream ss(line); 
       ss >> value; 
       std::cout << value << " " << f50(value) << std::endl;
    }
/*    do {
        std::cout << "Enter a number between 1 - 100: " << std::endl;
        std::cin >> value;
        std::cout << "Nb solution: ";  
        std::cout << f50(value) << std::endl;
    } while (value > 0);
*/
    return 0;
}


